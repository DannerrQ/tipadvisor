package tipadvisor.client.tipadvisor.client.signUp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import tipadvisor.client.R;
import tipadvisor.client.tipadvisor.client.login.Login;

public class signUp extends Activity {

    EditText forename, surname, email, password;
    Button signUp;
    String forenameText, surnameText, emailText, passwordText;
    ParseObject userTable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        //userTable = new ParseObject("User");

        forename = (EditText) findViewById(R.id.forenameField);
        surname = (EditText) findViewById(R.id.surnameField);
        email = (EditText) findViewById(R.id.emailField);
        password = (EditText) findViewById(R.id.passwordField);

        signUp = (Button) findViewById(R.id.register);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forenameText = forename.getText().toString().toLowerCase();
                surnameText = surname.getText().toString().toLowerCase();
                emailText = email.getText().toString().toLowerCase();
                passwordText = password.getText().toString().toLowerCase();

               // if(forenameText.equals("") || surnameText.equals("") || emailText.equals("") || passwordText.equals(""))
                  if(emailText.equals("") || passwordText.equals(""))
                    Toast.makeText(getApplicationContext()," Something has been left blank, please try again", Toast.LENGTH_LONG).show();
                else{
                    ParseUser user = new ParseUser();
                    user.setUsername(emailText);
                   // userTable.put("Forename", forenameText);
                    //userTable.put("Surname", surnameText);
                    user.setPassword(passwordText);
                    user.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                Toast.makeText(getApplicationContext(), "Congratulations You have signed up, please log in", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(signUp.this, Login.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "There seems to be an error connecting online, please try again", Toast.LENGTH_LONG).show();
                            }


                        }
                    });

                }

            }
        });
    }


    public void createCustomer() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("Forename", forenameText);
            jsonObject.put("Surname", surnameText);
            jsonObject.put("EmailAddress", emailText);

            //TODO: send to Danny
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

package tipadvisor.client.tipadvisor.client.BeaconSelector;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;

import tipadvisor.client.tipadvisor.client.payment.PaymentAmountActivity;

/**
 * Created by Phillium on 19/04/2015.
 */
public class SelectEstablishmentClickListener implements View.OnClickListener {

    private Beacon beacon;
    private Context context;
    private BeaconManager beaconManager;

    public SelectEstablishmentClickListener(BeaconManager beaconManager, Context context, Beacon beacon) {
       this.beaconManager = beaconManager;
       this.context = context;
       this.beacon = beacon;
    }

    public void onClick(View v) {
        Intent intent = new Intent(context, PaymentAmountActivity.class);
        intent.putExtra("beaconId", beacon.getMacAddress());
        beaconManager.disconnect();
        context.startActivity(intent);
    }
}

package tipadvisor.client.tipadvisor.client.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import tipadvisor.client.R;
import tipadvisor.client.tipadvisor.client.BeaconSelector.BeaconSelectorActivity;
import tipadvisor.client.tipadvisor.client.payment.PaymentAmountActivity;
import tipadvisor.client.tipadvisor.client.signUp.signUp;


public class Login extends Activity {

    Button login, signUp;
    EditText username, password;
    String usernameString, passwordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ParseUser.enableAutomaticUser();

        login = (Button) findViewById(R.id.loginButton);
        signUp = (Button) findViewById(R.id.signUpButton);

        username = (EditText) findViewById(R.id.login);
        username.setText("test@test.com");
        password = (EditText) findViewById(R.id.password);
        password.setText("test");


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usernameString = username.getText().toString().toLowerCase().trim();
                passwordString = password.getText().toString().toLowerCase().trim();

                ParseUser.logInInBackground(usernameString, passwordString, new LogInCallback(){
                    public void done(ParseUser parseUser, ParseException e) {
                        if (parseUser != null) {
                            Intent intent = new Intent(Login.this, BeaconSelectorActivity.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), usernameString + " You have succesfully logged in!", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getApplicationContext(), usernameString + " Does not exist!", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });

        signUp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, signUp.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

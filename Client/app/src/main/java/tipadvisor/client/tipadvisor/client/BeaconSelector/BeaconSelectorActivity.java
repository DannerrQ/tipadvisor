package tipadvisor.client.tipadvisor.client.BeaconSelector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.Utils;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import tipadvisor.client.R;
import tipadvisor.client.tipadvisor.client.Establishment;

public class BeaconSelectorActivity extends Activity {

    List<Beacon> beaconList = new ArrayList<Beacon>();
    ParseQuery<ParseObject> query;
    List<String> locationNames = new ArrayList<String>();
    List<String> averageList = new ArrayList<String>();

    List<Beacon> immediateBeacons = new ArrayList<Beacon>();
    List<Beacon> farBeacons = new ArrayList<Beacon>();

    Beacon theHolyGrail;

    private List<String> macAddresses = new ArrayList<String>();

    private static final String ESTIMOTE_PROXIMITY_UUID = "B9407F30-F5F8-466E-AFF9-25556B57FE6D";
    private static final Region ALL_ESTIMOTE_BEACONS = new Region("regionId", ESTIMOTE_PROXIMITY_UUID, null, null);
    private BeaconManager beaconManager = new BeaconManager(this);

    //private HashMap<String, Boolean> foundMac = new HashMap<String, Boolean>();
    //private List<String> foundBeaconsMacAddresses = new ArrayList<String>();
    private HashMap<String, String> estabToAvg = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beacon_selector);



        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override public void onBeaconsDiscovered(Region region, List<Beacon> beacons) {
                for (Beacon beacon : beacons) {
                    //if (!foundBeaconsMacAddresses.containsKey(beacon.getMacAddress())) {
                    if (!estabToAvg.containsKey(beacon.getMacAddress())) {
                        beaconList.add(beacon);
                        estabToAvg.put(beacon.getMacAddress(), getAvgTip(beacon));
                    }
                }
                if (estabToAvg.size() == 2) {
                    stopRanging();
                    beaconManager.disconnect();
                }
                doStuff();
            }
        });

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override public void onServiceReady() {
                try {
                    beaconManager.startRanging(ALL_ESTIMOTE_BEACONS);
                }catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void doStuff() {
        //work out which beacons are immediate or close
        for (Beacon beacon : beaconList) {
             Utils.Proximity proximity = Utils.computeProximity(beacon);
            // = Utils.Proximity.valueOf(ESTIMOTE_PROXIMITY_UUID);

            switch (proximity) {
                case IMMEDIATE: immediateBeacons.add(beacon);
                    break;
                case NEAR:      immediateBeacons.add(beacon);
                    break;
                case FAR:       farBeacons.add(beacon);
                    break;
            }
        }

        if (immediateBeacons.size() == 1) {
            theHolyGrail = immediateBeacons.get(0);
        }
        else if (immediateBeacons.size() > 1) {
            createSelection(immediateBeacons);
        }
        else if (immediateBeacons.size() == 0 ) {
            if (farBeacons.size() == 0) {
                Toast.makeText(getApplicationContext(), "There are no places available :(", Toast.LENGTH_LONG).show();
            }
            if (farBeacons.size() == 1) {
                theHolyGrail = farBeacons.get(0);
            }
            else if (farBeacons.size() > 1) {
                createSelection(farBeacons);
            }
        }
    }

    private String getEstablishmentName(Beacon beacon) {
        final Establishment estab = new Establishment();
        query = ParseQuery.getQuery("TipLocation");
        query.whereEqualTo("Beacon", beacon.getMacAddress());
//        query.findInBackground(new FindCallback<ParseObject>() {
//            public void done(List<ParseObject> parseQList, ParseException e) {
//                if (e == null) {
//                    for (ParseObject p : parseQList) {
//                        System.out.println(p.getString("Name"));
//                        estab.name = p.getString("Name");
//                    }
//                }
//            }
//        });

    try {
        if (estabToAvg.size() != 2) {
            List<ParseObject> objects = query.find();
            for (ParseObject p : objects) {
                estab.name = p.getString("Name");
            }
        }

        else return null;
    }
    catch (ParseException e) {
        e.printStackTrace();
    }

        if (estab.name != null) {
            return estab.name;
        }
        else {
            return null;
        }
    }

    private String getAvgTip(Beacon beacon){
        Double avg;
        String averageString = null;
        query = ParseQuery.getQuery("TipLocation");
        query.whereEqualTo("Beacon", beacon.getMacAddress());

        try {
            List<ParseObject> objects = query.find();
            for (ParseObject p : objects) {
               avg = p.getDouble("Average");
               averageString = String.valueOf(avg);

            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        if (averageString != null) {
            return averageString;
        }
        else {
            return null;
        }

    }

    private void createSelection(List<Beacon> beacons) {
        LinearLayout layout = (LinearLayout) findViewById(R.id.locationLayout);
        LinearLayout layout2 = (LinearLayout) findViewById(R.id.locationLayout2);

        for (Beacon beacon : beacons) {
            TextView textView = new TextView(this);
            TextView textView2 = new TextView(this);
            String estabName = getEstablishmentName(beacon);
            String avgTip = getAvgTip(beacon);
            if (estabName != null || avgTip != null) {
                boolean locationFound = false;
                boolean averageFound = false;
                if (locationNames.size() > 0) {
                    for (String location : locationNames) {
                        if (estabName.equals(location)) {
                            locationFound = true;
                        }
                    }
                }
                else if(averageList.size() > 0) {
                    for(String avg : averageList){
                        if(avgTip.equals(avg)){
                          averageFound = true;
                        }
                    }
                }

                if (!locationFound) {
                    locationNames.add(estabName);
                    textView.setText(estabName);
                    textView.setTextSize(35.0f);
                    textView.setGravity(Gravity.CENTER);
                    textView.setOnClickListener(new SelectEstablishmentClickListener(beaconManager, this, beacon));
                    layout.addView(textView);

                    avgTip = estabToAvg.get(beacon.getMacAddress());
                    textView2.setText(avgTip);
                    textView2.setTextSize(50.0f);
                    textView2.setGravity(Gravity.CENTER);
                    layout2.addView(textView2);
                }
            }
        }
    }

    private void stopRanging() {
        try {
            beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        stopRanging();
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_beacon_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

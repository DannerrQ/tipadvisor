﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Braintree;

namespace TipAdvisor.Models.BraintreeAPI
{
    public class Gateway
    {

        private static Gateway _self;

        private BraintreeGateway gateway;
        private string merchantId = "dv9qbwdjcj6dvyxt";
        private string publicKey = "f8kj9tkvtc22jp2n";
        private string privateKey = "652d8f0fe8c2ec89c2f902ba357208fc";

        public ClientTokenGateway ClientToken;
        public TransactionGateway Transaction;

        private Gateway()
        {
            gateway = new BraintreeGateway
            {
                Environment = Braintree.Environment.SANDBOX,
                MerchantId = merchantId,
                PublicKey = publicKey,
                PrivateKey = privateKey
            };
            ClientToken = gateway.ClientToken;
            Transaction = gateway.Transaction;
        }

        public static Gateway GetInstance()
        {
            if (_self == null)
            {
                _self = new Gateway();
            }
            return _self;
        }

    }
}
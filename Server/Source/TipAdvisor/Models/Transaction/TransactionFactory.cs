﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Parse;
using System.Threading.Tasks;

namespace TipAdvisor.Models.Transaction
{
    public static class TransactionFactory
    {

        public async static Task<Transaction> GetTransaction(string transactionId)
        {

            var query = from transaction in ParseObject.GetQuery("TipTransaction")
                        where transaction.ObjectId == transactionId
                        select transaction;
            IEnumerable<ParseObject> results = await query.FindAsync();

            ParseObject user = results.First<ParseObject>().Get<ParseObject>("User");
            ParseObject recipient = results.First<ParseObject>().Get<ParseObject>("Location");
            string nonce = results.First<ParseObject>().Get<string>("Nonce");
            double amount = results.First<ParseObject>().Get<double>("Amount");

            return new Transaction(user, recipient, nonce, amount);
        }

    }
}
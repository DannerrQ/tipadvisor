﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TipAdvisor.Models.Transaction
{
    public enum TransactionResult
    {
        NONE,
        SUCCESS,
        ERROR_VALIDATION,
        ERROR_DECLINED,
        ERROR_FRAUD,
        UNKNOWN
    }
}
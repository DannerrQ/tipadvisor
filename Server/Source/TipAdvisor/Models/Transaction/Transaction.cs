﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Parse;
using Braintree;

namespace TipAdvisor.Models.Transaction
{
    public class Transaction
    {

        private ParseObject customer;
        private ParseObject recipient;
        private string nonce;
        private double amount;

        private TransactionRequest request;
        private TransactionResult result;
        private string resultMessage;

        public Transaction(ParseObject customer, ParseObject recipient, string nonce, double amount)
        {
            this.customer = customer;
            this.recipient = recipient;
            this.nonce = nonce;
            this.amount = amount;
        }

        public void Submit()
        {

            var request = new TransactionRequest
            {
                Amount = Convert.ToDecimal(amount),
                PaymentMethodNonce = nonce,
                Options = new TransactionOptionsRequest
                {
                    SubmitForSettlement = true
                }
            };

            Result<Braintree.Transaction> result = BraintreeAPI.Gateway.GetInstance().Transaction.Sale(request);
            if (result.IsSuccess())
            {
                this.result = TransactionResult.SUCCESS;
                this.resultMessage = "Payment successful.";
            }
            else
            {
                if (result.Errors.Count > 0)
                {
                    this.result = TransactionResult.ERROR_VALIDATION;
                    this.resultMessage = "Payment details are not valid.";
                }
                else
                {

                    TransactionStatus status = result.Target.Status;
                    if (status == TransactionStatus.PROCESSOR_DECLINED)
                    {
                        this.result = TransactionResult.ERROR_DECLINED;
                        this.resultMessage = result.Target.ProcessorResponseCode + " : " + result.Target.ProcessorResponseText;
                    }
                    else if (status == TransactionStatus.SETTLEMENT_DECLINED)
                    {
                        this.result = TransactionResult.ERROR_DECLINED;
                        this.resultMessage = result.Target.ProcessorSettlementResponseCode + " : " + result.Target.ProcessorSettlementResponseText;
                    }
                    else
                    {
                        this.result = TransactionResult.UNKNOWN;
                        this.resultMessage = "Unknown status: " + status.ToString();
                    }

                }

            }

        }

        public ParseObject GetCustomer()
        {
            return customer;
        }

        public ParseObject GetRecipient()
        {
            return recipient;
        }

        public double GetAmount()
        {
            return amount;
        }

        public void SetAmount(int newAmount)
        {
            amount = newAmount;
        }

        public TransactionResult GetResult()
        {
            return result;
        }

        public string GetResultMessage()
        {
            return resultMessage;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TipAdvisor.Controllers
{
    public class ClientController : ApiController
    {

        public string GetClientKey()
        {
            return Models.BraintreeAPI.Gateway.GetInstance().ClientToken.generate();
        }

    }
}
